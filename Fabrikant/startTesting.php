<?php

$suites = getopt('', ['suites:']);
$now = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
$dateNow = date("d.m.y", $now->getTimestamp());
foreach($suites as $key => $suite){
 
    $suite = preg_replace('/(\s)/', '', $suite);
    $suites[$key] = explode(',', $suite);
    
}

foreach($suites['suites'] as $key => $suiteName){
    
    require_once(__DIR__ . "/Tests/Suites/$suiteName.php");
    
    foreach($suite as $num => $step){
        $flag = TRUE;
        exec('php ' . __DIR__ . "/lib/forker.php --step $num --suite $suiteName > " . __DIR__ . "/log/errLog.txt");
        if(!file_exists(__DIR__ . "/lib/log_$dateNow.txt")){
            $handle = fopen(__DIR__ . "/lib/log_$dateNow.txt", 'a');
            fclose($handle);
        }
        while($flag){
            $lines = file(__DIR__ . "/lib/log_$dateNow.txt");
            if(count($lines) == ($num + 1)){
                break;
            }
        }
    }
    unlink(__DIR__ . "/lib/log_$dateNow.txt");  
}
      

