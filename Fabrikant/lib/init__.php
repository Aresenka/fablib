<?php
require_once(__DIR__ . '/../../lib/__init__.php');
require_once __DIR__ . '/../Configuration.php';
require_once('Classes/CreateProcedure.php');
require_once('Classes/CreateAndSearch.php');
require_once('Classes/Driver.php');
$options = getopt('', ['browser:']);
$dateNow = date("Y-m-d_H:i:s", strtotime("+3 hour"));
$browser = $options['browser'];
$host = $hosts[$browser];
switch($browser){
                    
                    case 'IE9':
                    case 'IE10':
                    case 'IE11':
                        $capabilities = DesiredCapabilities::internetExplorer();
                        break;
                    case 'EDGE':
                        $capabilities = DesiredCapabilities::microsoftEDGE();
                        break;
                    case 'firefox':
                        $capabilities = DesiredCapabilities::firefox();
                        break;
                    case 'chrome':
                        $capabilities = DesiredCapabilities::chrome();
                        break;
                    case 'safari':
                        $capabilities = DesiredCapabilities::safari();
                        break;
                }

$driver = Driver::create($host, $capabilities, 120*1000, 120*1000);
echo "$dateNow: " . posix_getpid() . "is $browser child PID \n";
