<?php

class Forker{
    
    public $step;
    public $suite;
    
    public $stepParams;
    public $file;
    
    public function getOpt(){
        
        $options = getopt('', ['step:', 'suite:']);
        $this->step = $options['step'];
        $this->suite = $options['suite'];
        
        }
    
    public function getParams($suitePath){
        
        require_once $suitePath;
        $this->stepParams = $suite[$this->step];
        $this->file = $this->stepParams['file'];
        
    }
    
    private function getCurrentTime($stamp){
        
        $now = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
        return date($stamp, $now->getTimestamp());
        
    }
    
    public function fork($DIR, $browser, $key){
        
        switch($pid = pcntl_fork()){
            
            case -1:
                throw new Exception("Can not fork");
                break;
            case 0:
                exec("php $DIR/../Tests/Files/$this->file.php --browser $browser > $DIR/../log/" . $this->getCurrentTime('d.m.y') . "_$this->file.txt");
                if(($key + 1) == count($this->stepParams['browsers'])){
                    posix_kill(posix_getppid(), SIGUSR1);
                }
                exit();
                break;
            default:
                break;
        
        }
    }
    
    public function forkAll($DIR, $hosts){
        
        $counter = 1;
        foreach($hosts as $browser => $host){
            switch($pid = pcntl_fork()){
                case -1:
                    throw new Exception("Can not fork");
                    break;
                case 0:
                    exec("php $DIR/../Tests/Files/$this->file.php --browser $browser > $DIR/../log/" . $this->getCurrentTime('d.m.y_H:i:s') . "_$this->file.txt");
                    if($counter == count($hosts)){
                        posix_kill(posix_getppid(), SIGUSR1);
                    }
                    exit();
                    break;
                default :
                    break;
            }
            $counter++;
        }
        
        
    }
    
    
    
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

