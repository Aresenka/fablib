<?php

class CreateProcedure{
    
    private $driver;
    public $procNum;
    public $timeCreated;
    public $searchKey;
    public $sell = "0";
    


    public function __construct(
            $driver
            ){
        
        $this->driver = $driver;
    }
    
    private function customFill20($data){
        foreach($data as $field){
            if(array_key_exists('by', $field)){
                $selector = explode(':', $field['by']);
            }
            switch($field['type']){
                case 'input':
                    $this->fillInput($selector, $field);
                    break;
                case 'firmOrUser':
                    $this->fillFirmOrUser($field);
                    break;
                case 'select':
                    $this->fillSelect($selector, $field);
                    break;
                case 'checkbox':
                    $this->fillCheckbox($selector);
                    break;
                case 'okpd2':
                    $this->fillOKPD2($field);
                    break;
                case 'dates':
                    $this->fillDates($field);
                    break;
                case 'okato':
                    $this->fillOKATO($field);
                    break;
                default:
                    echo 'Undefined type'.PHP_EOL;
                    break;
            }
            sleep(1);
        }
    }
    
    private function fillInput($selector, $data){
        $input = $data['input'];
        $this->driver->findBy($selector[0], $selector[1])->clear()->sendKeys($input);
    }
    
    private function fillFirmOrUser($data){
        $what = ucfirst($data['what']);
        $buttons = $this->driver->findAllBy('className', 'show'.$what.'Dialog');
        foreach($buttons as $button){
            if($button->isDisplayed()){
                $button->click();
            }
        }
        sleep(10);
        $list = $this->driver->findAllBy('className', 'choose_firm');
        $displayed = [];
        foreach($list as $check){
            if($check->isDisplayed()){
                $displayed[] = $check;
            }
        }
        $displayed[$data['number']]->click();
    }
    
    private function fillSelect($selector, $data){
        $list = $this->driver->findAllBy($selector[0], $selector[1]);
        $list[$data['number']]->click();
    }
    
    private function fillCheckbox($selector){
        $check = $this->driver->findAllBy($selector[0], $selector[1]);
        foreach($check as $checkbox){
            if($checkbox->isDisplayed()){
                $checkbox->click();
            }
        }
    }
    
    private function fillOKPD2($data){
        if(!empty($this->driver->findAllBy('className', 'label-danger'))){
            $this->driver->findBy('className', 'label-danger')->click();
        }
        $this->driver->findBy("className", "os_okpd2_run")->click();
        sleep(2);
        $this->driver->findBy("name", 'search_by_okpd2_num')->sendKeys($data['code']);
        $this->driver->findBy("className", 'search_button_okpd2select')->click();
        sleep(10);
        $this->driver->findBy("className", 'okpd_code_select')->click();
        sleep(2);
    }
    
    private function fillDates($data){
        $dates = $this->driver->findAllBy('className', 'datetimepicker');
        $increment = explode('.',$data['increment']);
        $time = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
        if($data['quantity'] == 'all'){
            foreach($dates as $date){
                $time->add(new DateInterval('P'
                . $increment[2] .'Y'
                . $increment[1] .'M'
                . $increment[0] .'D'
                .'T'
                . 0 .'H'
                . 0 .'M'));
                $date->clear()->sendKeys($time->format("d.m.Y H:i"));
            }
        }else{
            $numbers = explode(',', $data['quantity']);
            foreach($numbers as $number){
                $time->add(new DateInterval('P'
                . $increment[2] .'Y'
                . $increment[1] .'M'
                . $increment[0] .'D'
                .'T'
                . 0 .'H'
                . 0 .'M'));
                $dates[$number - 1]->clear()->sendKeys($time->format("d.m.Y H:i"));
            }
        }
    }
    
    private function fillOKATO($data){
        $this->driver->findBy('className', 'fias_okato_run_lot_delivery_place_okato_')->click();
        $codes = $this->driver->findBy('className', 'okato_code');
        $codes[$data['number']]->click();
    }
    
    /**
     * Use to fill all inputs, found by $findBy, with $withWhat string
     * Look customFill() to $findBy mask details
     * 
     * @param RemoteWebDriver $driver
     * @param string $findBy Mask "searchBy:Val"
     * @param type $withWhat What you like to put into inputs
     */
    
    public static function fillAll($driver, $findBy, $withWhat){
        
        $findAll = explode(":", $findBy);
        $textFields = $driver->findAllBy($findAll[0], $findAll[1]);
        foreach($textFields as $field){
            if($field->isDisplayed()){
                $field->sendKeys($withWhat);
            }
        }
        
    }
    
    /**
     * Use to click all elements, found by $findBy
     * Look customFill() to $findBy mask details
     * 
     * @param RemoteWebDriver $driver
     * @param string $findBy Mask "searchBy:Val"
     */
    
    public static function clickAll($driver, $findBy){
        
        $findAll = explode(":", $findBy);
        $checkBox = $driver->findAllBy($findAll[0], $findAll[1]);
        foreach($checkBox as $box){
            if($box->isDisplayed()){
                $box->click();
            }
        }
        
    }
    
    public static function abandon($driver, $date){
        
        $driver->findBy('partialLinkText', 'Отказаться')->click();
        sleep(2);
        CreateProcedure::fillAll($driver, 'className:form-control', 'test');
        CreateProcedure::dateFill($driver, $date);
        $driver->findBy("name", 'is_draft')->click();
        sleep(2);
        $driver->findBy("partialLinkText", 'Опубликовать')->click();
    }
    
    public function create($data){
        if($data['type'] == '2.0'){
            $this->driver->get($data['link']);
            $this->autoFill20();
            if(!empty($data['custom'])){
                $this->customFill20($data['custom']);
            }
            $this->driver->findBy('name', 'save')->click();sleep(10);
            $this->downloadDoc20($data['documentation']);
            $this->driver->findBy('linkText', 'Опубликовать на ЭТП')->click();
            $timeCreated = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
            preg_match(
                "/№(.*)\)/", 
                $this->driver->findBy("className", 'panel-heading')->getText(), 
                $match);
            $this->procNum = $match[1];
            $this->timeCreated = $timeCreated->getTimestamp();
            $this->sell = $data['sell'];
        }else{
            
        }
    }
    
    private function autoFill20(){
        $this->driver->findBy('linkText', 'Включить дебаг')->click();sleep(10);
        for($i = 0; $i <3; $i++){
            $test = $this->driver->findAllBy('linkText', 'Заполнить извещение');
            if(!empty($test)){
                $this->driver->findBy('linkText', 'Заполнить извещение')->click();sleep(15);
                break;
            }else{
                $this->driver->navigate()->refresh(); 
                sleep(10);
            }
        }
    }
    
    private function downloadDoc20($document){
        for($i = 0; $i < 3; $i++){
            $link = $this->driver->findAllBy('linkText', 'Загрузить документацию');
            if(!empty($link)){
                $link[0]->click();sleep(3);
                $this->driver->findBy('name', 'procedure_document_file')->sendKeys($document['source']);
                $this->driver->findBy('name', 'procedure_document_title')->sendKeys($document['title']);
                $this->driver->findBy('name', 'save')->click();
                break;
            }else{
                throw new Exception('Something wrong with documentation!');
            }
        }
    }
    
}

