<?php

class CreateAndSearch{
    
    /**
     * Use to create procedure with custom type
     * 
     * @param RemoteWebDriver $driver
     * @param array $params Array with configuration data
     * @return type
     */
    
    public static function create($driver, $params){
        
        $what = "Create" . $params["type"];
        $procedure = CreateProcedure::$what($driver, $params);
        return $procedure;
        
        
    }
    
    /**
     * Use to match, how much time need to find
     * procedure after creation. It is ~3 seconds less
     * because of sleep time
     * 
     * @param RemoteWebDriver $driver
     * @param array $params Array with configuration data
     * @return string Time is needed to find created procedure
     */
    
    public static function getTime($driver, $params){
        
        $createdProc = CreateAndSearch::create($driver, $params);
        
        $driver->get('http://' . $params["server"] . '.fabrikant.ru/trades/procedure/search/?type=' . $createdProc->sell . '&query=' . $createdProc->searchKey . '&procedure_stage=0&price_from=&price_to=&currency=0&date_type=date_publication&date_from=&date_to=&ensure=all&count_on_page=10&order_by=default&order_direction=1');
        sleep(0.5);
        preg_match("/([0-9]{1,})/", $driver->findBy('className', 'Search-result-count')->getText(), $count);
        $found = $count[1];
        
        if($found != "1"){
            while($found != "1"){
                $driver->get('http://' . $params["server"] . '.fabrikant.ru/trades/procedure/search/?type=' . $createdProc->sell . '&query=' . $createdProc->searchKey . '&procedure_stage=0&price_from=&price_to=&currency=0&date_type=date_publication&date_from=&date_to=&ensure=all&count_on_page=10&order_by=default&order_direction=1');
                sleep(0.5);
                preg_match("/([0-9]{1,})/", $driver->findBy('className', 'Search-result-count')->getText(), $count);
                $found = $count[1];
            }
        }
        
        
        $now = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
        $timeFound = date("i:s", ($now->getTimestamp() - $createdProc->timeCreated));
        
        return $timeFound;
        
    }
    
}

