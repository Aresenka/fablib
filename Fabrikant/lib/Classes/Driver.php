<?php

class Driver extends RemoteWebDriver{
    
    
    
    /**
    * Light version of findElement function to search elements by:
    * id,       className,       name,    cssSelector, 
    * linkText, partialLinkText, tagName, xpath
    * 
    * @param string $what By what we will search?
    * @param string $val  Value of search method $what
    * @return RemoteWebElement NoSuchElementException is thrown in
    *    HttpCommandExecutor if no element is found.
    */
    public function findBy($what, $val){
        return $this->findElement(WebDriverBy::$what($val));
        }    
    
    /**
     * Find all WebDriverElements within the current page using the given
     * mechanism - light version.
     * @param string $what
     * @param string $val
     * @return RemoteWebElement[] A list of all WebDriverElements, or an empty
     * array if nothing matches
     */
    public function findAllBy($what, $val){
        return $this->findElements(WebDriverBy::$what($val));
    }
    
    /**
     * Scroll webpage to selected element
     * @param type $driver instance of created Driver
     * @param type $element instance of WebDriverElement
     */    
    public function scrollTo($element){
        if(is_object($element)){
            $y = $element->getLocation()->getY();
        }else{
            $y = $element;
        }

        $this->executeScript("window.scrollTo(0, $y)");
        
    }
    
    /**
     * This function calculated width, height and position (x and y)
     * of selected $element. Y-position calculating with reference to
     * window position on the page, when X-position is calculating with 
     * reference to page.
     * @param type $element WebDriverElement instance
     * @return array [width, height, position_x, position_y]
     */
    public function getElementSizeAndPlace($element){
        
        $scrollY = $this->executeScript('return window.scrollY');
        $elementY = $element->getLocation()->getY();
        $resultY = $elementY - $scrollY;

        $element_width = $element->getSize()->getWidth();
        $element_height = $element->getSize()->getHeight();

        $element_src_x = $element->getLocation()->getX();
        $element_src_y = $resultY;
        
        $sizeAndPlace = [$element_width, 
                         $element_height, 
                         $element_src_x, 
                         $element_src_y];
        
        return $sizeAndPlace;
    }
    
    /**
     * Take screenshot of full page or (if it needed)
     * of selected $element
     * @param type $path path, which will be used to save
     * created screenshot in .png format, need to be like
     * /path/to/screenshot/name
     * @param type $element WebDriverElement object, founded by RemoteWebDriver
     * @return string the full path of created screenshot, like a
     * /path/to/screenshot/name.png
     * @throws Exception if file with such name as $path exist, it will
     * not save screenshot and throw the exceptions
     */
    public function screenshot($path, $element=null) {
        if( ! (bool) $element) {
            return $this->takeScreenshot($path.'.png');
        }else{
            $el_placeAndSize = $this->getElementSizeAndPlace($element);                            //Вычисляем его положение и размеры
            $window_h = $this->executeScript('return document.documentElement.clientHeight');   //Стягиваем высоту окна, она же будет высотой скриншота
            $body_h = $this->executeScript('return document.body.scrollHeight');              //Стягиваем высоту всей страницы
            $this->scrollTo($element);                                                             //Скроллим до элемента
            $i=0;                                                                               //Определяем счётчик созданных скриншотов
            $big = false;                                                                       //Определяем флаг большого элемента
            while($el_placeAndSize[1] >= $window_h){                                                                        //Запускаем цикл 
                    $this->takeScreenshot($path.$i.'.png');                             //Делаем скриншот
                    `convert $path$i.png -crop $el_placeAndSize[0]x$window_h+$el_placeAndSize[2]+0 $path$i.png`;    //Обрезаем его по высоте экрана и ширине элемента
                    $el_placeAndSize[1] -= $window_h;                                                      //Вычисляем, сколько осталось заскринить,
                    $el_placeAndSize[3] += $window_h;                                                       //и куда скроллить дальше
                    $this->scrollTo($el_placeAndSize[3]);                                               //И скроллим
                    $i++;                                                                       //Ещё скриншот богу скриншотов!
                    $big = true;                                                                //Элемент большой
            }
            if($el_placeAndSize[1] > 0){
                $this->takeScreenshot($path.$i.'.png');                                 //Делаем скриншот
                sleep(2);                                                                       //Ждём, пока он оформится по всем канонам
                $cut_from = $window_h + $el_placeAndSize[3] - $body_h;
                `convert $path$i.png -crop $el_placeAndSize[0]x$el_placeAndSize[1]+$el_placeAndSize[2]+$cut_from $path$i.png`;
            }
            `montage $path{0..$i}.png -tile 1x -geometry +0+0 $path.png`;                       //И лепим из всех один большой
            `rm -r $path{0..$i}.png`;
        }
        if(!file_exists($path.'.png')) {
            throw new Exception('Could not save screenshot');
        }
    }
    
    /**
     * Compare $file1 with $file2, both placed in $filesPath,
     * and put resulting file in the $resPath folder
     * (if it is not declared, it will be the $filesPath folder)
     * @param type $file1 the name of first file, which is need to be
     * an .png file, ex.: 
     * /path/to/file1.png -> ...comapreFiles('file1',...
     * @param type $file2 the name of second file, same as the first one
     * @param type $filesPath the full path to the folder, which contains
     * both of $file1 and $file2 files, WITHOUT closing slash, ex.:
     * /full/path/to/file1ANDfile2.png -> ...compareFiles($file1, $file2, '/full/path/to',...
     * @param string $resPath is null by defaults, but if you need to place
     * resulting file in whatever place, then put full path of this place here,
     * WITH closing slash, ex.:
     * /full/path/to/pathWith/results.png -> ...,$filesPath, '/full/path/to/pathWith/')...
     * @return string the full name with path of resulting file, ex.:
     * compareFiles('first', 'second', '/full/path/', '/result/path') -> '/result/path/first-second.png'
     */
    public function compareFiles($file1, $file2, $filesPath, $resPath = null){
        
        if(! (bool) $resPath){
            $resPath = "$filesPath/";
        }
        
        $pathToFile1 = "$filesPath/$file1.png";
        $pathToFile2 = "$filesPath/$file2.png";
        
        `compare $pathToFile1 $pathToFile2 $resPath$file1-$file2.png`;
        
        return "$resPath$file1-$file2.png";
    }
    
    /**
     * Compare files (.png) in $samples folders with same files in the 
     * 'Reference' folder (names of files, which is need to be compared,
     * is need to be the same, ex.:
     * /path/to/samples/sample.png && /path/to/Reference/sample.png;
     * Of course, it is need to have a Reference folder with reference
     * files in it. The Reference folder must be in that folder, in
     * what samples folders is, and test.php file with this function
     * in it must be in that folder too.
     * @param array $samples an array with names of folders with samples
     * to compare
     * @param type $DIR __DIR__ constant only
     * If the Result folder is not exist, it will be created,
     * and results will be saved in that folder with the same name.png
     */
    public function compareWithReference(array $samples, $DIR){
        
        $pathRes = "$DIR/Result/";
        if(!file_exists($pathRes)){
            
            mkdir($pathRes);
            
        }
        
        foreach($samples as $dir){
            
            $dirRef = "$DIR/Reference/";
            $refFiles = glob("$dirRef*.png");
            $dirSample = "$DIR/$dir/";
            
            foreach(glob("$dirSample*.png") as $samplePng){
                
                $name = explode($dirSample, $samplePng);
                $resName = "$dir\_$name[1]";
                
                foreach($refFiles as $refPng){
                    
                    $nameRef = explode($dirRef, $refPng);
                    if($name[1] == $nameRef[1]){
                        
                        `compare $samplePng $refPng $pathRes$resName`;
                        break;
                    }
                }
            }
        }
    }
    
    /**
     * Compare files (.png) in the two folders ($dir1 and $dir2).
     * Files to compare must have same names, resulting files will 
     * be saved in Result folder, which will be created as well, if
     * it does not exist.
     * Folders to compare and executed test file must be in $path folder.
     * @param type $dir1 name of the first directory without closing slash
     * @param type $dir2 name of second directory without closing slash
     * @param type $path full path to comapred folders and test file without
     * closing slash.
     */
    public function compareWith($dir1, $dir2, $path){
        
        $pathRes = "$path/Result/";
        
        if( !file_exists($pathRes)){ 
 
            mkdir($pathRes);
    
        }
        
        $path1 = "$path/$dir1/";
        $path2 = "$path/$dir2/";
        $files1 = glob("$path1*.png");
        $files2 = glob("$path2*.png");
        
        foreach($files1 as $sample){
    
            $sampleName = explode($path1, $sample);

            foreach($files2 as $compare){

                $compareName = explode($path2, $compare);

                if($sampleName[1] == $compareName[1]){

                    `compare $sample $compare $pathRes$dir1-$dir2\_$sampleName[1]`;

                }

            }

        }
        
    }
    
    /**
     * Use to log in on the fabrikant site
     * If log in failed, window will be closed with error text
     * 
     * @param RemoteWebDriver $driver
     * @param string $server
     * @param string $login
     * @param string $password
     */
    
    public function logIn($account){
        $this->get('http://test.fabrikant.ru/trades/procedure/search/');
        
        $this->findBy('className', 'signin')->click();    
        $this->findBy("id", "f_login")->sendKeys($account['login']);
        $this->findBy("id", 'f_password')->sendKeys($account['password']);
        $this->findBy('id', 'signinButton')->click();
        sleep(5);
        
    }
    
    /**
     * Use to log out on the fabrikant site
     * 
     * @param RemoteWebDriver $driver
     */
    
    public static function logOut(){
        
        $this->findBy("linkText", "exit")->click();
        sleep(2);
        
    }
}

