<?php

$data = [
    'type' => '2.0',
    'link' => 'http://test.fabrikant.ru/v2/trades/configurator/simple/new-create-procedure/HEtG51xb3mDsj0oUjEFhBw', //Ссылка на создание процедуры для быстрого создания
    'account' => [
        'login' => 'IronMan',
        'password' => 'Test123',
    ],
    'documentation' => [
        'title' => 'test',
        'source' => 'C:\test.txt',],
    'sell' => 1,
    'custom' => [
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text56d8321b9f315', //№ сообщения на ЕФРСБ
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7aa05aa211', //Фамилия арбитражного (финансового) управляющего 
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7aa10071b8', //Имя арбитражного (финансового) управляющего 
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7aa18203b0', //Отчество арбитражного (финансового) управляющего
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7aa25c5c8e', //ИНН арбитражного (финансового) управляющего
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_textarea55a7aa498afb8', //СРО, членом которого является арбитражный (финансовый) управляющий
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7aa2ba7093', //Номер дела о банкротстве
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7aa1e283a4', //Наименование арбитражного суда, рассматривающего дело о банкротстве
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'firmOrUser',
            'what' => 'user',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:.procedure_debtor[choice][key] option',//Должник 
            'number' => 2, //Порядковый номер, начиная с нуля
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:procedure_debtor[juridicalFullName]', //Полное наименование
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:procedure_debtor[juridicalShortName]', //Краткое наименование
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:procedure_debtor[juridicalOGRN]', //ОГРН
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:procedure_debtor[sharedINN]', //ИНН
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_procedure_text55a7c53e79271', //Заказчик
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'dates', //all - заполняет все поля даты, инкремируя значение для каждого на указанный промежуток
            'quantity' => 'all', //[0-9]{1,}, - заполняет указанные поля, инкремируя значение для каждого (пример: 1,3,5)
            'increment' => '10.0.0', //значение инкремента, д.м.г (пример: 1.2.5 инкремирует на 1 день, 2 месяца и 5 лет)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:proposal_stages[0][price]', //Цена
            'input' => '1488',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:name', //Предмет договора
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7af00b993f', //Месторасположение предмета торгов
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7aef35580f', //Порядок ознакомления с имуществом
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b15841095', //Условия передачи имущества
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7af147a8de', //Условия оплаты имущества
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b1641a4dc', //Порядок и сроки заключения договора купли/продажи
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'okpd2',
            'code' => '07.29', //Код ОКПД2
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b336e50d5', //Порядок регистрации на сайте
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b368f1a8c', //Порядок подачи заявок
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b393048bd', //Документы, прилагаемые к заявке
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7af24a0bd7', //Размер, сроки и порядок внесения задатка. Реквизиты счетов на которые вносится задаток
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b17d4a242', //Порядок оформления участия в торгах
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b39dcf63a', //Порядок и критерии выявления победителя
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b2fdd4f05', //Место проведения
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7c186c5d70', //Место подведения результатов торгов
            'input' => 'autotext',
        ],//*/
    ]
];

