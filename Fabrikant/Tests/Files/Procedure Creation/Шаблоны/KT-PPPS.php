<?php

$data = [
    'type' => '2.0',
    'link' => 'http://test.fabrikant.ru/v2/trades/configurator/simple/new-create-procedure/CltzIi7tcOrYnHBH5108sA', //Ссылка на создание процедуры для быстрого создания
    'account' => [
        'login' => 'IronMan',
        'password' => 'Test123',
    ],
    'documentation' => [
        'title' => 'test',
        'source' => 'C:\test.txt',],
    'sell' => 1,
    'custom' => [
        /*
        [
            'type' => 'firmOrUser',
            'what' => 'user',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b1641a4dc', //Заказчик
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:name', //Предмет договора
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7af00b993f', //Краткое описание
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'okpd2',
            'code' => '07.29', //Код ОКПД2
        ],//*/
        /*
        [
            'type' => 'dates', //all - заполняет все поля даты, инкремируя значение для каждого на указанный промежуток
            'quantity' => 'all', //[0-9]{1,}, - заполняет указанные поля, инкремируя значение для каждого (пример: 1,3,5)
            'increment' => '10.0.0', //значение инкремента, д.м.г (пример: 1.2.5 инкремирует на 1 день, 2 месяца и 5 лет)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:proposal_stages[0][price]', //Общее наименование процедуры
            'input' => '10,50',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7aef35580f', //Условия поставки
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7b15841095', //Условия оплаты
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea55a7af147a8de', //Сроки выполнения предмета договора
            'input' => 'autotext',
        ],//*/
    ]
];

