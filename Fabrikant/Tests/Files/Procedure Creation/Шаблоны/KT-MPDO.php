<?php

$data = [
    'type' => '2.0',
    'link' => 'http://test.fabrikant.ru/v2/trades/configurator/simple/new-create-procedure/OHIWd-bMXCamL0bf3EDj0A', //Ссылка на создание процедуры для быстрого создания
    'account' => [
        'login' => 'fabrikant',
        'password' => 'Qqqq111!',
    ],
    'documentation' => [
        'title' => 'test',
        'source' => 'C:\test.txt',],
    'sell' => 0,
    'custom' => [
        /*
        [
            'type' => 'firmOrUser',
            'what' => 'user',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'dates', //all - заполняет все поля даты, инкремируя значение для каждого на указанный промежуток
            'quantity' => '1,2', //[0-9]{1,}, - заполняет указанные поля, инкремируя значение для каждого (пример: 1,3,5)
            'increment' => '10.0.0', //значение инкремента, д.м.г (пример: 1.2.5 инкремирует на 1 день, 2 месяца и 5 лет)
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:.currency option',
            'number' => 4, //Порядковый номер, начиная с нуля, валюты в селекте
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:procedure_common_name', //Общее наименование процедуры
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea555c38fd5d286', //Регион поставки
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:name', //Предмет договора
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea546058d79c6f8', //Краткое описание продукции/услуг
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea5460598c6c3b3', //Условия оплаты
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_textarea546059513915d', //Условия доставки
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'okpd2',
            'code' => '07.29', //Код ОКПД2
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[no_price]', //Начальная цена: Без указания цены
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[no_nds]', //Начальная цена: НДС не облагается. Раскомментить, если требуется указать НДС
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[view_without_nds]', //Начальная цена: Без НДС
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[with_nds]', //Начальная цена: Цена c НДС. Указывать её, если
            'input' => '4071505', //активирован ввод НДС (после введения НДС изменяется цена без НДС)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[without_nds]', //Начальная цена: Цена без НДС. Указывать, если
            'input' => '4071505',//НДС не вводится (галки без НДС и НДС не облагается скрывают цену с НДС)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[nds]', //Начальная цена: НДС
            'input' => '14,88',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_quantity[quantity]', //Количество
            'input' => '1',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_quantity[units]', //Единицы измерения
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price_per_unit[no_price]', //Цена за единицу: Без указания цены
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price_per_unit[no_nds]', //Цена за единицу: НДС не облагается. Раскомментить, если требуется указать НДС
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price_per_unit[view_without_nds]', //Цена за единицу: Без НДС
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price_per_unit[with_nds]', //Цена за единицу: Цена c НДС. Указывать её, если
            'input' => '4071505', //активирован ввод НДС (после введения НДС изменяется цена без НДС)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price_per_unit[without_nds]', //Цена за единицу: Цена без НДС. Указывать, если
            'input' => '4071505',//НДС не вводится (галки без НДС и НДС не облагается скрывают цену с НДС)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price_per_unit[nds]', //Цена за единицу: НДС
            'input' => '14,88',
        ],//*/
    ]
];

