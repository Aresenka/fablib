<?php

$data = [
    'type' => '2.0',
    'link' => 'http://test.fabrikant.ru/v2/trades/configurator/simple/new-create-procedure/lL3Uau_7QpMzU-3WwXsbRw', //Ссылка на создание процедуры для быстрого создания
    'account' => [
        'login' => 'IronMan',
        'password' => 'Test123',
    ],
    'documentation' => [
        'title' => 'test',
        'source' => 'C:\test.txt',],
    'sell' => 1,
    'custom' => [
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'procedure_customer[choice][key]\'] option',//Тип заказчика
            'number' => 1, //Порядковый номер типа заказчика в селекте
        ],//*/
        /*
        [
            'type' => 'firmOrUser',
            'what' => 'firm',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'firmOrUser',
            'what' => 'user',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:name', //Предмет договора
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'okpd2',
            'code' => '07.29', //Код ОКПД2
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'lot_currency[key]\'] option', //Валюта
            'number' => 4, //Порядковый номер, начиная с нуля, валюты в селекте
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[no_nds]', //Начальная цена: НДС не облагается
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[view_without_nds]', //Начальная цена: без НДС 
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[without_nds]', //цена без НДС
            'input' => '1488',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[nds]', //НДС
            'input' => '20',
        ],//*/
        /*
        [
            'type' => 'dates', //all - заполняет все поля даты, инкремируя значение для каждого на указанный промежуток
            'quantity' => 'all', //[0-9]{1,}, - заполняет указанные поля, инкремируя значение для каждого (пример: 1,3,5)
            'increment' => '10.0.0', //значение инкремента, д.м.г (пример: 1.2.5 инкремирует на 1 день, 2 месяца и 5 лет)
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionDownStage[priceStop][price]', //Этап понижения: цена отсечения
            'input' => '1000',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionDownStage[stepLow][value]', //Шаг понижения
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionDownStage[stepLow][type]\'] option',//Шаг понижения селект
            'number' => 0, //0 - %; 1 - сумма
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionDownStage[intervalLow][value]', //Интервал понижения
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionDownStage[intervalLow][interval]\'] option',//Интерва понижения селект
            'number' => 4, //0 - мин, 1 - ч, 2 - д
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'name:DutchAuctionDownStage[proposalCondition]',//Количество подтверждений цены
            'number' => 0, //0 - одно подтверждение, 1 - несколько подтверждений
        ],//*/
        
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionUpStage[intervalExpectation][value]', //Интервал ожидания начала этапа повышения
            'input' => '5',
        ],
        
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionUpStage[intervalExpectation][interval]\'] option',//Интервал ожидания - селект
            'number' => 1, //0 - мин, 1- ч, 2 - д
        ],
        
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionUpStage[stepProposal]', //Кратность шага аукциона
            'input' => '2',
        ],
        
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionUpStage[stepSizeProposal][value]', //Шаг аукциона
            'input' => '3',
        ],
        
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionUpStage[stepSizeProposal][type]\'] option',//Шаг аукциона - селект
            'number' => 0, //0 - %; 1 - сумма
        ],
        /*
        [
            'type' => 'select',
            'by' => 'name:DutchAuctionUpStage[extensionInBet]',//Продление при подаче ставки
            'number' => 1, //0 - от даты подачи ставки; 1 - от даты завершения аукциона
        ],//*/
        
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionUpStage[stepExtension][value]', //Шаг продления
            'input' => '4',
        ],
        
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionUpStage[stepExtension][interval]\'] option',//Шаг продления - селект
            'number' => 1, //0 - мин, 1- ч, 2 - д
        ],
        
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionUpStage[intervalUp][value]', //Длительность этапа повышения
            'input' => '7',
        ],
        
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionUpStage[intervalUp][interval]\'] option',//Длительность этапа повышения - селект
            'number' => 0, //0 - мин, 1- ч, 2 - д
        ],
        
        [
            'type' => 'input',
            'by' => 'name:DutchAuctionUpStage[timeToExtension][value]', //Время до завершения этапа
            'input' => '20',
        ],
        
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'DutchAuctionUpStage[timeToExtension][interval]\'] option',//Время до завершения этапа - селект
            'number' => 2, //0 - мин, 1- ч, 2 - д
        ],
    ]
];

