<?php

$data = [
    'type' => '2.0',
    'link' => 'http://test.fabrikant.ru/v2/trades/configurator/simple/new-create-procedure/X1I8Z3HvZh0Bd7F7lOWbiw', //Ссылка на создание процедуры для быстрого создания
    'account' => [
        'login' => 'IronMan',
        'password' => 'Test123',
    ],
    'documentation' => [
        'title' => 'test',
        'source' => 'C:\test.txt',],
    'sell' => 0,
    'custom' => [
        /*
        [
            'type' => 'input',
            'by' => 'name:procedure_common_name', //Наименование закупки
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:1_procedure_YesNo5849462dc6da6', //Закупка осуществляется вследствие аварии...
        ],//*/
        /*Не отлажено до конца, в этом типе процедур вызывает баги, лучше не трогать
        [
            'type' => 'firmOrUser',
            'what' => 'user',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'procedure_customer[choice][key]\'] option',//Тип заказчика
            'number' => 1, //Порядковый номер типа заказчика в селекте
        ],//*/
        /*Не отлажено до конца, в этом типе процедур вызывает баги, лучше не трогать
        [
            'type' => 'firmOrUser',
            'what' => 'firm',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*Не отлажено до конца, в этом типе процедур вызывает баги, лучше не трогать
        [
            'type' => 'firmOrUser',
            'what' => 'user',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 3, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:procedure_rnp', //Требование к отсутствию участников закупки в реестре недобросовестных поставщиков
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'procedure_detached_custom[detached_custom_opf_form]\'] option',//Обособленное подразделение
            'number' => 1, //Порядковый номер типа заказчика в селекте
        ],//*/
        /*Не отлажено до конца, в этом типе процедур вызывает баги, лучше не трогать
        [
            'type' => 'firmOrUser',
            'what' => 'firm',//firm для выбора организации из справочника, user для выбора контактного лица
            'number' => 0, //Порядковый номер, начиная с нуля, контактного лица в модальном окне
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_text584947302bed4', //Номер плана закупки
            'input' => '1232131',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:1_lot_text58494d328f5e5', //Номер позиции плана
            'input' => '312312',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:name', //Предмет договора 
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[no_price]', //Начальная цена: Без указания цены
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[no_nds]', //Начальная цена: НДС не облагается
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:lot_price[view_without_nds]', //Начальная цена: Без НДС
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[without_nds]', //Начальная цена: Цена без НДС
            'input' => '4071505',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_price[nds]', //Начальная цена: НДС
            'input' => '14,88',
        ],//*/
        /*
        [
            'type' => 'select',
            'by' => 'cssSelector:[name=\'lot_currency[key]\'] option', //Валюта
            'number' => 4, //Порядковый номер, начиная с нуля, валюты в селекте
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:1_lot_YesNo58494a4d3b4f8', //Закупка не включена в план закупки в соответствии...
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:1_lot_YesNo58494ab21e1b9', //Участниками закупки могут быть только субъекты малого и среднего предпринимательства
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:1_lot_YesNo58494add5f010', //В отношении участников закупки установлено требование о привлечении к исполнению...
        ],//*/
        /*
        [
            'type' => 'checkbox',
            'by' => 'name:1_lot_YesNo58494b2f3926f', //Закупка не учитывается в соответствии с пунктом...
        ],//*/
        /*
        [
            'type' => 'okato',//Код региона по ОКАТО
            'number' => 10,//Порядковый номер, начиная с нуля
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_delivery_place[state]', //Субъект РФ\Федеральный округ
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_delivery_place[region]', //Субъект РФ\Регион
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_delivery_place[address]', //Место поставки товара, работ, услуг
            'input' => 'autotext',
        ],//*/
        /*
        [
            'type' => 'okpd2',
            'code' => '07.29', //Код ОКПД2
        ],//*/
        /*
        [
            'type' => 'input',
            'by' => 'name:lot_okei[quantity]', //Количество
            'input' => '1',
        ]//*/
    ]
];

