<?php

require_once __DIR__ . '/../../../lib/init__.php';
require __DIR__. '/../Procedure Creation/Шаблоны/TPB-PPP.php';

$driver->logIn($data['account']);
$procedure = new CreateProcedure($driver);
$procedure->create($data);
var_dump($procedure);/*
$driver->quit();

/*
CreateProcedure::logIn($driver, 'test', 'IronMan', 'Test123');
$driver->get($data['link']);
$driver->findBy('linkText', 'Включить дебаг')->click();sleep(10);
for($i = 0; $i <3; $i++){
    $test = $driver->findAllBy('linkText', 'Заполнить извещение');
    if(!empty($test)){
        $driver->findBy('linkText', 'Заполнить извещение')->click();sleep(15);
        break;
    }else{
        $driver->navigate()->refresh(); 
        sleep(10);
    }
}

if(!empty($data['custom'])){
    foreach($data['custom'] as $arr){
        if(array_key_exists('by', $arr)){
            $selector = explode(':', $arr['by']);
        }
        switch($arr['type']){
            case 'input':
                $input = $arr['input'];
                $driver->findBy($selector[0], $selector[1])->clear()->sendKeys($input);
                echo 'input'.PHP_EOL;
                break;
            case 'contactUser':
                $driver->findBy('className', 'showUserDialog')->click();sleep(10);
                $list = $driver->findAllBy('className', 'choose_firm');
                $list[$arr['number']]->click();
                echo 'contactUser'.PHP_EOL;
                break;
            case 'select':
                $list = $driver->findAllBy($selector[0], $selector[1]);
                $list[$arr['number']]->click();
                echo 'select'.PHP_EOL;
                break;
            case 'checkbox':
                $check = $driver->findAllBy('name', 'lot_price[no_price]');
                foreach($check as $checkbox){
                    if($checkbox->isDisplayed()){
                        $checkbox->click();
                    }
                }
                echo 'checkbox'.PHP_EOL;
                break;
            case 'okpd2':
                if(!empty($driver->findAllBy('className', 'label-danger'))){
                    $driver->findBy('className', 'label-danger')->click();
                }
                $driver->findBy("className", "os_okpd2_run")->click();
                sleep(2);
                $driver->findBy("name", 'search_by_okpd2_num')->sendKeys($arr['code']);
                $driver->findBy("className", 'search_button_okpd2select')->click();
                sleep(2);
                $driver->findBy("className", 'okpd_code_select')->click();
                sleep(2);
                break;
            case 'dates':
                $dates = $driver->findAllBy('className', 'datetimepicker');
                $increment = explode('.',$arr['increment']);
                
                $time = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
                if($arr['quantity'] == 'all'){
                    foreach($dates as $date){
                        $time->add(new DateInterval('P'
                        . $increment[2] .'Y'
                        . $increment[1] .'M'
                        . $increment[0] .'D'
                        .'T'
                        . 20 .'H'
                        . 0 .'M'));
                        $date->clear()->sendKeys($time->format("d.m.Y H:i"));
                        sleep(1);
                    }
                }else{
                    $numbers = explode(',', $arr['quantity']);
                    foreach($numbers as $number){
                        $time->add(new DateInterval('P'
                        . $increment[2] .'Y'
                        . $increment[1] .'M'
                        . $increment[0] .'D'
                        .'T'
                        . 20 .'H'
                        . 0 .'M'));
                        $dates[$number - 1]->clear()->sendKeys($time->format("d.m.Y H:i"));
                        sleep(1);
                    }
                }
                break;
            default:
                echo 'Undefined type'.PHP_EOL;
                break;
        }
        sleep(1);
    }
}

var_dump(empty($data['custom']));

/*
$driver->get("http://test.fabrikant.ru/trades/procedure/search/");
CreateProcedure::logIn($driver, 'test', 'IronMan', 'Test123');
$driver->findBy('className', 'create_procedure')->click();sleep(30);
$driver->findBy('linkText', 'МПДО')->click();sleep(30);
//$driver->get('http://test.fabrikant.ru/market/edit.html?type=360&action=create_pdo_multi&firm_id=43546');

$driver->findBy('className', 'showUserDialog')->click();
$list = $driver->findAllBy('className', 'choose_firm');
$user = 3;
$list[$user]->click();
$dates = $driver->findAllBy('className', 'datetimepicker');
$time = new DateTime('NOW', new DateTimeZone('Europe/Moscow'));
$time->add(new DateInterval('P'
        . 10 .'Y'
        . 5 .'M'
        . 20 .'D'
        .'T'
        . 13 .'H'
        . 45 .'M'));

foreach($dates as $field){
    $field->clear()->sendKeys($time->format("d.m.Y H:i"));
    $time->add(new DateInterval("P1D"));
    sleep(1);
}

$options = $driver->findAllBy("cssSelector", '.currency option');
$currency = 4;
$options[$currency]->click();

$inputData = [
    'name:procedure_common_name' => 'Test autoname common',
    'name:1_lot_textarea555c38fd5d286' => 'Test autoregion',
    'name:name' => 'Test autoname',
    'name:1_lot_textarea546058d79c6f8' => 'Test autodescription',
    'name:1_lot_textarea5460598c6c3b3' => 'Test autoconditions of payments',
    'name:1_lot_textarea546059513915d' => 'Test autoconditions of delivery',
];
foreach($inputData as $selector => $input){
    $selectorArr = explode(':', $selector);
    $driver->findBy($selectorArr[0], $selectorArr[1])->sendKeys($input);
}

$okpd = '07.29';
$driver->findBy("className", "os_okpd2_run")->click();
sleep(2);
$driver->findBy("name", 'search_by_okpd2_num')->sendKeys($okpd);
$driver->findBy("className", 'search_button_okpd2select')->click();
sleep(2);
$driver->findBy("className", 'okpd_code_select')->click();
sleep(1);

$price = [
    'checkbox' => [
        'name:lot_price[no_price]' => FALSE,
        'name:lot_price[no_nds]' => FALSE,
        'name:lot_price[view_without_nds]' => FALSE
    ],
    'data' => [
        'name:lot_price[without_nds]' => '4071505',
        'name:lot_price[nds]' => '20',
        'name:lot_quantity[quantity]' => '1',
        'name:lot_quantity[units]' => 'Test autounit',]
];

$priceContainers = $driver->findAllBy('className', 'price-box');
$noPrice = 0;
$ndsFlag = 0;
    foreach($price['checkbox'] as $select => $bool){
        if($bool){
            $selectArr = explode(':', $select);
            $arr = $driver->findBy($selectArr[0], $selectArr[1])->click();
            $noPrice = preg_match('(no_price)', $select);
            $ndsFlag = preg_match('(nds)', $select);
        }
    }
    if(!$noPrice){
        foreach($price['data'] as $select => $input){
            $isNDS = preg_match('(nds)', $select);
            $selectArr = explode(':', $select);
            if(!$ndsFlag){
                if($isNDS){
                    $driver->findBy($selectArr[0], $selectArr[1])->sendKeys($input);
                }
            }
            $driver->findBy($selectArr[0], $selectArr[1])->sendKeys($input);
        }
    }
$driver->findBy('name', 'save')->click();/*
sleep(20);
$driver->quit();

/*
$testarr = $driver->findAllBy('xpath', '//input');
foreach($testarr as $obj){
    if($obj->isDisplayed()){
        $obj->sendKeys('Testtest123');
    }
}
$testSelect = $driver->findAllBy('xpath', '//select');
foreach($testSelect as $select){
    if($select->isDisplayed()){
        var_dump('Name of select: '.$select->getAttribute('name'));
    }
}

//$driver->quit();

/*
$driver->findBy('linkText', 'Включить дебаг')->click();sleep(3);
$driver->findBy('linkText', 'Заполнить извещение')->click();sleep(3);
$driver->findBy('name', 'save')->click();sleep(3);
$driver->findBy('linkText', 'Загрузить документацию')->click();sleep(3);
$driver->findBy('name', 'procedure_document_file')->sendKeys('C:\test.txt');
$driver->findBy('name', 'procedure_document_title')->sendKeys('test');
$driver->findBy('name', 'save')->click();
$driver->findBy('linkText', 'Опубликовать на ЭТП')->click();sleep(30);

$driver->quit();

/*
$driver->get('https://www.fabrikant.ru/trades/procedure/search/');
$driver->findBy("className", "date-string")->isDisplayed();
sleep(5);
var_dump($driver->findBy("className", "date-string")->isDisplayed());
//$driver->executeScript('document.getElementsByClassName("date-string")[0].style.visibility = "hidden"');
$driver->screenshot(__DIR__.'/withoutDate');
$driver->quit();


/*
$driver->get("https://varlamov.ru");
$el = $driver->findBy('id', 'home'); 
$driver->screenshot(__DIR__.'/test');
$driver->quit();
/*
$driver->get("https://www.fabrikant.ru/trades/procedure/search/?type=0&section_type[]=ds500&date_type=date_publication&currency=0&order_by=date_publication&order_direction=1&page=1");
$html = $driver->findBy('cssSelector', 'html');
var_dump(is_object(42));
$driver->close();

/*$i=0;
while(true){
    
    echo $i;
    $i++;
    if($i == 5){
        break;
    }
}
echo PHP_EOL."хуй";
/*
$first = 'image1.png';
$second = 'image2.png';
`montage $first $second -tile 1x2 -geometry +0+0 result.png`;
$driver->close();

$driver->get("https://varlamov.ru");
$html_h = $driver->executeScript('return document.documentElement.clientHeight');   //Стягиваем высоту окна, она же будет высотой скриншота
$el = $driver->findBy('id', 'home');                                    //Стягиваем нужный для скриншота элемент
$el_placeAndSize = $driver->getElementSizeAndPlace($el);                            //Вычисляем его положение и размеры
$el_h = $el_placeAndSize[1];                                                        //Присваиваем высоту элемента переменной
$scrollTo = $el_placeAndSize[3];                                                    //Присваиваем у-координату переменной для первого скролла и последующих вычислений
$body_h = $driver->executeScript('return document.body.scrollHeight');              //Стягиваем высоту всей страницы
$check_h = $body_h - $scrollTo;                                                     //Вычисляем высоту страницы без проскроленной части
$curr_el_h = $el_h;                                                                 //Определяем переменную размера оставшейся части элемента
$driver->scrollTo($el);                                                             //Скроллим до элемента
$i=0;                                                                               //Определяем счётчик созданных скриншотов
$big = false;                                                                       //Определяем флаг большого элемента
while($curr_el_h >= $html_h){                                                                        //Запускаем цикл 
        $driver->takeScreenshot(__DIR__.'/path'.$i.'.png');                             //Делаем скриншот
        `convert path$i.png -crop $el_placeAndSize[0]x$html_h+$el_placeAndSize[2]+0 path$i.png`;    //Обрезаем его по высоте экрана и ширине элемента
        $curr_el_h -= $html_h;                                                      //Вычисляем, сколько осталось заскринить,
        $scrollTo += $html_h;                                                       //и куда скроллить дальше
        $driver->scrollTo($scrollTo);                                               //И скроллим
        $i++;                                                                       //Ещё скриншот богу скриншотов!
        $big = true;                                                                //Элемент большой
}
if($curr_el_h > 0){
    $driver->takeScreenshot(__DIR__.'/path'.$i.'.png');                                 //Делаем скриншот
    sleep(2);                                                                       //Ждём, пока он оформится по всем канонам
    if(($body_h - $scrollTo)<$html_h){
        $cut_from = $html_h + $scrollTo - $body_h;
        `convert path$i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from path$i.png`;
    }
}
`montage path{0..$i}.png -tile 1x -geometry +0+0 path.png`;                       //И лепим из всех один большой
`rm -r path{0..$i}.png`;
/*if($check_h == $el_h){                                                      //Если элемент прижат к нижнему краю страницы
    $cut_from = $html_h - ($el_h%$html_h);                                  //Посчитаем, сколько могло налезть предыдущего скриншота на нынешний: посчитаем остаток от деления высоты элемента на высоту экрана (столько останется, после полноэкранных съёмок) и отнимем его от высоты экрана (столько лишнего будет сверху скриншота дублировать предыдущий скриншот)
    `convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from $i.png`;//Обрежем
    //var_dump("convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from $i.png");
}
if($big){
    //$cut_from = $html_h - ($el_h%$html_h);
    //`convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from $i.png`;
    //`convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+0 $i.png`;
    //var_dump("convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from $i.png");
}else{
    `convert $i.png -crop $el_placeAndSize[0]x$el_placeAndSize[1]+$el_placeAndSize[2]+0 $i.png`;
}
if($big){                                                   //Но если всё-таки пришлось сделать кучу скриншотов
     //То смотрим, какой вообще длины страница
    if($check_h == $el_h){
        $cut_from = $html_h - ($el_h%$html_h);
        //`convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from $i.png`;
        //var_dump("convert $i.png -crop $el_placeAndSize[0]x$curr_el_h+$el_placeAndSize[2]+$cut_from $i.png");
    }
    
}
echo "html_h = $html_h".PHP_EOL;
echo "body_h = $body_h".PHP_EOL;
echo "check_h = $check_h".PHP_EOL;
echo "scrollTo = $scrollTo".PHP_EOL;
echo "el_h = $el_h".PHP_EOL;
echo "cut_from = $cut_from".PHP_EOL;
echo "curr_el_h = $curr_el_h ".PHP_EOL;





$driver->quit();
echo "closed";

/*
while(TRUE){
    $dateNow = date("Y-m-d_H:i:s", strtotime("+3 hour"));
    $driver->get("https://www.fabrikant.ru/trades/procedure/search/?type=0&section_type[]=ds500&date_type=date_publication&currency=0&order_by=date_publication&order_direction=1&page=1");
    $driver->screenshot("/home/master/RosAtomSBT/$dateNow");
    sleep(57);
}
    /*
    $el = $driver->findBy('className', 'marketplace-result');
    $driver->screenshot(__DIR__.'/test', $el);
    $monitor = new WebDriverWindow();
    var_dump($driver->getElementSizeAndPlace($el));
    var_dump($driver->getEx());
    $driver->close();

/*
$exe = 1;
$counter = 0;
$test = curl_init();
$url = 'http://test.fabrikant.ru/v2/trades/configurator/simple/new-create-procedure/OHIWd-bMXCamL0bf3EDj0A';
while($exe == 1){
    try{
        $exe = 0;
        $driver->get($url);
        echo 'test', PHP_EOL;
        CreateProcedure::createMPDO($driver, $params);
    }
    catch(Exception $e){
        curl_setopt($test, CURLOPT_URL, $url);
        curl_setopt($test, CURLOPT_NOBODY, TRUE);
        curl_exec($test);
        switch(curl_getinfo($test, CURLINFO_HTTP_CODE)){
            case '200':
                try{
                    $driver->findBy('className', 'error500');
                    if($counter <5){
                        $exe = 1;
                        echo 'Ошибка 500. Пытаемся ещё ' . (5 - $counter) . ' раз', PHP_EOL;
                        $counter++;
                        break;
                    }
                    else{
                        echo 'Ошибка 500. Пропускаем кейс.', PHP_EOL;
                    }
                } catch (Exception $ex) {
                    echo $e->getMessage(), PHP_EOL, 'Пропускаем кейс.', PHP_EOL;
                    break;
                }
            case '404':
                echo 'Страница не найдена. Пропускаем кейс.', PHP_EOL;
                break;
            case '403':
                echo 'Доступ запрещён. Пропускаем кейс.', PHP_EOL;
                break;
            case '502':
                if($counter <5){
                        $exe = 1;
                        echo 'Ошибка 502. Пытаемся ещё ' . (5 - $counter) . ' раз', PHP_EOL;
                        $counter++;
                        break;
                    }
                    else{
                        echo 'Ошибка 502. Пропускаем кейс.', PHP_EOL;
                    }
                break;
        }
        echo curl_getinfo($test, CURLINFO_HTTP_CODE), PHP_EOL;
    }
}
 echo "Всё ок", PHP_EOL;


/*
$driver->close();
 
$driver->compareWith('Reference', 'Firefox', __DIR__);


/*
$pathRes = __DIR__ . '/Result/';

if( !file_exists($pathRes)){ 
 
    mkdir($pathRes);
    
}

$dir = 'Firefox';

$dirRef = __DIR__ . '/Reference/';
$refFiles = glob($dirRef . '*.png');

$dirSample = __DIR__ . '/' . $dir . '/';

foreach(glob($dirSample . '*.png') as $png){
    
    $name = explode($dirSample, $png);
    echo 'Name: ' . $name[1] . PHP_EOL;
    $resName = $dir . '_' . $name[1];
    
    foreach($refFiles as $refPng){
        $nameRef = explode($dirRef, $refPng);
        echo 'Name Reference: ' . $nameRef[1] . PHP_EOL;
        if($name[1] == $nameRef[1]){
            `compare $png $refPng $pathRes$resName`;
            break;
        }
    }
    
}

/*
$searchPage = 'http://test.fabrikant.ru/trades/procedure/search/';
$driver->get($searchPage);
sleep(2);

$element = $driver->findBy('id', 'search_query');
$element->sendKeys('test');

$driver->scrollTo($element);
sleep(2);

$firstScr = $driver->screenshot('/home/master/vendor/facebook/webdriver/Fabrikant/Tests/test1', $element);

$element->clear()->sendKeys('test моржов');

$driver->scrollTo($element);
sleep(2);

$secondScr = $driver->screenshot('/home/master/vendor/facebook/webdriver/Fabrikant/Tests/test2', $element);


/*
$element->getLocationOnScreenOnceScrolledIntoView();
/*$location = $element->getLocation();

$window->setPosition($location);

echo $window->getPosition()->getX() . PHP_EOL;
echo $window->getPosition()->getY() . PHP_EOL;*/