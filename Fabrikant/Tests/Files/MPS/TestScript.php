<?php
require_once (__DIR__ . '/../../../lib/init__.php');
echo posix_getpid() . " is TestScript PID", PHP_EOL;

$length = 5;
pcntl_sigprocmask(SIG_BLOCK, [SIGUSR1]);
for($counter = 1; $counter <= $length; $counter++){
    
    switch($pid = pcntl_fork()){
        case -1:
            //@ERROR
            echo "Error while trying to create", PHP_EOL;
            break;
        case 0:
            //@CHILD
            echo posix_getpid() . " is a $counter child;", PHP_EOL;
            sleep(30);
            if($counter == $length){
                posix_kill(posix_getppid(), SIGUSR1);
            }
            exit();
            break;
        default:
            //@PARENT
            break;
    }
    
    
}
pcntl_sigwaitinfo([SIGUSR1]);
fwrite(fopen(__DIR__ . '/test.csv', 'a'), "$mainCount test is done" . PHP_EOL);
/*
$options = getopt('', ['suites::', 'files:']);

foreach($options as $key => $value){
    $value = preg_replace('/(\s)/', '', $value);
    $options[$key] = explode(',', $value);
}

var_dump($options);
/*
$options = getopt('a:b:w:');
$a = $options['a'];
$b = $options['b'];
$w = $options['w'];
sleep($w);
$math[0] = $a + $b;
$handle = fopen(__DIR__ . '/test.csv', 'a');
fputcsv($handle, $math);

/*
 * 
 */
