<?php

require_once __DIR__ . '/../../lib/init__.php';

$searchPage = 'http://' . $params['server'] . '.fabrikant.ru/trades/procedure/search/';
$driver->get($searchPage);
$count = 0;
$filtersCount = $driver->findAllBy('className', 'Search-filter-title');
foreach($filtersCount as $key => $value){
    
    $driver->findBy('id', 'clear_submit')->click();
    sleep(3);
    $filters = $driver->findAllBy('className', 'Search-filter-title');
    $filter = $filters[$count];
    $text = explode(PHP_EOL, $filter->getText());
    $filter->click();
    sleep(3);
    $matchAll = explode(' ', $driver->findBy('className', 'Search-result-count')->getText());
    echo $text[0] . '; MatchAll: ' . $matchAll[1] . PHP_EOL;
    if($matchAll[1] == "0"){
        echo PHP_EOL . 'ERROR: Процедуры не найдены!' . PHP_EOL;
        $path = __DIR__ . '/ErrorBy' . $text[0];
        $driver->screenshot($driver, $path, $driver->findBy('className', 'main'));
    }
    else if($params['server'] != 'www'){
        $driver->findBy('id', 'search_query')->clear()->sendKeys('KPIT-autotest')->submit();
        sleep(3);
        $match = explode(' ', $driver->findBy('className', 'Search-result-count')->getText());
        echo $text[0] . '; Match: ' . $match[1] . PHP_EOL;
        if($text[0] != 'ГОСЗАКУПКИ'){
            if($match[1] == "0"){
                echo PHP_EOL . 'ERROR: Процедура не найдена!' . PHP_EOL;
                $path = __DIR__ . '/ErrorBy' . $text[0];
                $driver->screenshot($driver, $path, $driver->findBy('className', 'main'));
            }
        }
    }
    echo '###############################' . PHP_EOL;
    $count++;
}