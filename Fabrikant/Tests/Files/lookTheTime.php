<?php

require_once __DIR__ . '/../init__.php';

CreateProcedure::logIn($driver, 
                      $params['server'], 
                      $params['login'], 
                      $params['password']);
$summ = 0;

for($i = 1; $i <= 5; $i++){
    
    $time[$i-1] = CreateAndSearch::getTime($driver, $params);
    echo $i . ' time: ' . $time[$i-1] . PHP_EOL;
    $summ = $summ + $time[$i-1];
    
}

$summ = $summ/5;
echo 'Average time is ' . $summ . PHP_EOL;
